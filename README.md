## mup-aws-elb

Plugin for Meteor Up to deploy using AWS Elastic Load Balancers and EC2.

### Why not just use [mup-aws-beanstalk](https://github.com/zodern/mup-aws-beanstalk)?
If you have multiple applications at the same domain, e.g. http://example.com/app1 and http://example.com/app2 you need to share a load balancer. mup-aws-beanstalk doesn't (and possibly can't) support this.

Similarly, if you have multiple unrelated apps using Beanstalk, you end up with 1 load balancer per project. This can become expensive ($16/balancer/month/AZ). Using [Advanced Configurations](./docs/advanced.md) you can share a single load balancer.

### Why not just use [raw meteor-up](https://github.com/zodern/meteor-up)?
1. meteor-up can result in significant (30-60 seconds) downtime, even when multiple servers are used, depending on your configuration (ELB drainage, health check frequency and number of failures) due to the targets still being listed by the load balancer).
2. meteor-up requires the definition of all servers, and thus can't support auto-scaling.

### Features:
- Load balancing with support for sticky sessions and web sockets
- Meteor settings.json
- Zero downtime deploys
- Automatically uses the correct node version (curtesy of meteor-up)
- Spinup more instances, either on deploy (to maintain capacity) or standalone (to increase capacity)
- Deploy to all live instances, no server definitions required
- Configure ELB, listeners, target groups and instances

### Current Status
this plugin is a work in progress, currently functional:

- Creating a new load balancer/listeners/target groups with http/https listeners
- Spinning up new EC2 instances from a set of definitions
- Setting up and deploying to these new instances - if appropriate security groups are manually created, and their ID's specified
- Deploying to these instances
- Registering these new instances to the target group, and waiting for their healthchecks
- Deploying to existing instances in a rolling update, waiting for each to be healthy before deploying the next

Currently in progress:

- Re-configuring the load balancer/listeners/target groups based on changes
- Re-configuring the meteor/docker environment on deploy
- Creating new/modifying existing security groups
- Spinning down instances

Roadmap:

- Green/Blue deployment based on either swapping out target groups, or swapping out targets within a group.
- Auto-scaling
- Controlling where new instances get put (which availability zones)

[Documentation](./docs/index.md)
