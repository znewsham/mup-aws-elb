## Install

You can install `mup` and `mup-aws-elb` by running

```bash
npm i -g mup mup-aws-elb
```

The AWS ELB plugin requires Node 4 or newer and Meteor Up 1.3.5 or newer.

## Step 1: Initialize your project

In the terminal, run

```
cd path/to/app
mkdir .deploy && cd .deploy
mup init
```

This will create a .deploy folder in your app, and `mup init` will add a Meteor settings file and mup config in it.

## Step 2: Customize your Mup Config

You can replace the mup config in `.deploy/mup.js` with this:

```js
module.exports = {
    app: {
        // Tells mup that the AWS Beanstalk plugin will manage the app
        type: 'aws-elb',
        name: 'myApp',
        path: '../',
        env: {
            ROOT_URL: 'http://app.com',
            MONGO_URL: 'mongodb://user:pass@domain.com'
        },
        auth: {
            id: '12345',
            secret: '6789',
            instancePem: "~/.ssh/aws.pem",
            instanceUsername: "ubuntu"
        }
    },
    plugins: ['mup-aws-elb']
};
```

You will want to modify:
1) The app name. It must be at least 4 characters
2) `app.env.ROOT_URL`
3) `app.env.MONGO_URL` You will need to get a database from mLab, Compose, or another DBaaS provider

The next step will provide the values for the `app.auth` object.

## Step 3: Create AWS user

You will need to [create an Amazon account](https://portal.aws.amazon.com/billing/signup#/start) if you do not have one.

Next, create an IAM user at [https://console.aws.amazon.com/iam/home?region=us-east-1#/users](https://console.aws.amazon.com/iam/home?region=us-east-1#/users)

The access type should be `Programmatic access`.
You can select `Add user to group` and create a new group. The group should have the following permissions:

- `AWSEC2FullAccess`

In your mup config, set `app.auth.id` to the Access Key ID, and `app.auth.secret` to the Secret access key AWS gives you after creating the user.

## Step 4: Deploy

run:

```
mup elb deploy
#you will need to apply appropriate security groups to the new instances to :
  1. allow access on your application port
  2. allow SSH in from your deployment machine
mup elb setup
mup elb deploy
```

It will setup and deploy your app.
