module.exports = {
  plugins: ['mup-aws-elb'],
  hooks: {
  	'pre.meteor.build': {
      localCommand: '...'
    }
  },
  servers: {},
  app: {
    servers: {},
    type: 'aws-elb',
    name: 'app-name',
    deployment: {
      balancer: {
        securityGroups: ['balancer-group-name'],
        name: 'balancer-name',
        ssl: {
          certificateArn: '...'
        },
        tags: [
          // any tags for the balancer
        ],
        // NOTE: remember that priorities are global within a load balancer
        //      changes here should be checked with ALL other apps using the same load balancer
        rules: [
          {
            conditions: [{
              field: 'path-pattern',
              value: '/app-name'
            }],
            priority: '5'
          }
        ]
      },

      // number of base instances (just used for setup, could in the future be used for autoscaling)
      baseInstances: 2,
      launchTemplate: {
        imageId: 'ami-imageId',
        instanceType: 't2.small',
        keyName: 'ssh-key-name',
        instanceName: 'app-name',
        blockDeviceMappings: [
          {
            deviceName: '/dev/sda1',
            ebs: {
              deleteOnTermination: true,
              volumeSize: 16
            }
          }
          // others as necessary
        ],
        // can be a group name (e.g., will attach to an existing group, or create a new one)
        // or a config with a name, and the names of groups to allow comms from
        securityGroups: [
          'a-group-name',
          {
            groupName: 'instance-group-name',
            inboundGroupNames: ['balancer-group-name']
          }
        ],
        tags: [
          // any tags for the instances
        ]
      }
    },
    path: 'path-to-app',
    region: 'us-west-2',
    auth: {
      // path to ssh key to login
      instancePem: ' ',
      // username of instance
      instanceUsername: 'ubuntu',

      // aws credentials to manaage ELB/EC2
      id: '',
      secret: ''
    },

    buildOptions: {
      serverOnly: true
    },

    env: {
    },

    docker: {
      buildInstructions: [
        //        'RUN apt-get update && apt-get install -y pkg-config imagemagick libcairo2-dev libjpeg62-turbo-dev libpango1.0-dev libgif-dev build-essential g++'
      ],
      // change to 'abernix/meteord:base' if your app is using Meteor 1.4 - 1.5
      image: '',
      args: [
      ]
    },

    // Show progress bar while uploading bundle to server
    // You might need to disable it on CI servers
    enableUploadProgressBar: true
  }
};
