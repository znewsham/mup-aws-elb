## Getting Started

Read the [Getting Started tutorial](./getting-started.md).
See some [Advanced Configurations](./advanced.md).

## Config

- This plugin is configured with the `app` object in your config.
- `app.type` should be set to `aws-elb` so Meteor Up knows this plugin will deploy and manage the app.
- You will need to add `mup-aws-elb` to the `plugins` array in your config
- The `servers` object needs to be present, and should be `{}`

Example config:

```js
module.exports = {
    app: {
        type: 'aws-elb',
        // Must be at least 4 characters
        name: 'app-name',
        path: '../',
        auth: {
            // IAM user's Access key ID
            id: '12345',
            // IAM user's Secret access key
            secret: '6789',

            //the credentials to access your EC2 instance
            instanceUsername: "ec2-user",
            instancePassword: "my-password",
            instancePem: "~/.ssh/aws.pem"
        },
        region: "us-east-1",
        env: {
            ROOT_URL: 'http://website.com',
            MONGO_URL: 'mongodb://user:pass@domain.com'
        },
        deployment: {
          balancer: {
            //configuration/identification for the balancer,listeners and target groups
            securityGroups: [
              "group-name",
              "sg-groupId",
              {
                groupName: "new-balancer-group-name",
                //configuration for a new security group
              }
            ],
            //all fields are optional, (though security groups is recommened) default configuration will work for most meteor apps
            name: "app-name",
            //will cause an HTTPS listener to be created
            ssl: {
              certificateArn: "my-cert-name"
            },
            tags: [{
              key: "mykey",
              value: "myvalue"
            }]
          },
          //how many instances should be running at all times - will be spun up as necessary
          baseInstances: 2,
          launchTemplate: {
            //used to spinup new instances as required
            imageId: "ami-4e79ed36", //NOTE: currently not supported.
            instanceType: "t2.micro",
            keyName: "aws",//the public key name on record with AWS that mathces instancePem specified above
            instanceName: "name-of-new-instances",
            securityGroups: [
              "group-name",
              "sg-groupId",
              {
                groupName: "new-group-name",
                //configuration for a new security group
                inboundGroupNames: ["new-balancer-group-name"]
              }
            ],
            tags: [{
              key: "akey",
              value: "avalue"
            }]
          }
        }
    },
    plugins: ['mup-aws-elb']
}
```

## Commands

- `mup elb deploy` Deploys to all existing servers for the selected target group
- `mup elb spinupInstances --instances=2` Creates new instances according to your launch template
- `mup elb setup` Setup meteor and docker for all instances, called on spinupInstances.
- `mup elb cleanup` Remove unhealthy, terminated  (or optionally not) instances from the target group
- `mup elb servers` lists the servers currently running, and their health statuses
- `mup elb balancer` returns the configuration (including listeners and target groups) of the current load balancer
- `mup elb reconfig` create or modify a load balancer with associated listeners and target groups

## Rolling Deploys

When deploying a new version, there is no downtime, however, currently the number of servers handling requests IS reduced by 1.

## Load balancing

Load balancing is automatically configured and supports web sockets and sticky sessions.

## Scale
To scale your app, modify `app.baseInstances` in your mup config to how many servers you want your app deployed to. Then run `mup elb reconfig`.

Alternatively, run `mup elb spinupInstances --instances=2`.

Currently you can only scale up, not down.

AWS limits how many instances your account can have. To increase the limit, you will need to contact their customer support. Learn more [here](https://aws.amazon.com/premiumsupport/knowledge-center/manage-service-limits/).
