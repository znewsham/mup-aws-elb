import { expect } from 'chai';
import { ensureBaseInstances } from '../src/command-handlers';
import prepareConfig from '../src/prepareConfig';
import { ec2, elbv2 } from '../src/aws.js';
import configure from '../src/aws.js';
import sinon from 'sinon';
describe('spinupInstances', () => {
  before(function(){
    configure({auth: {}, region: ""});
  });
  function getApi(options, balancerConfig = {}){
    return {
      getBasePath(){
        return "./";
      },
      resolvePath(){
        return "";
      },
      commandHistory:["elb.deploy"],
      getConfig(){
        return prepareConfig({
          app: {
            name: "test",
            docker: {
              prepareBundle: true
            },
            auth: {
              instanceUsername: "ubuntu",
            },
            deployment: {
              launchTemplate: {

              },
              balancer: balancerConfig
            }
          }
        });
      },
      getOptions(){
        return options;
      }
    }
  }
  let stubs = [];
  beforeEach(function(){
    stubs = [];
  });
  afterEach(function(){
    stubs.forEach(stub=>stub.restore());
  });

  it("should return the load balancer when referenced by arn", function(done){
    done();
  });
});
