import { expect } from 'chai';
import { cleanup } from '../src/command-handlers';
import prepareConfig from '../src/prepareConfig';
import { ec2, elbv2 } from '../src/aws.js';
import configure from '../src/aws.js';
import sinon from 'sinon';
describe('cleanup', () => {
  before(function(){
    configure({auth: {}, region: ""});
  });
  function getApi(options){
    return {
      getConfig(){
        return prepareConfig({
          app: {
            name: "test",
            deployment: {
              launchTemplate: {

              },
              balancer: {

              }
            }
          }
        });
      },
      getOptions(){
        return options;
      }
    }
  }
  let stubs = [];
  beforeEach(function(){
    stubs = [];
  });
  afterEach(function(){
    stubs.forEach(stub=>stub.restore());
  })
  it('should NOT deregister instances when they are healthy', (done) => {
    stubs.push(sinon.stub(elbv2, "describeLoadBalancers").returns({
      promise(){
        return Promise.resolve({
          LoadBalancers: [{
            LoadBalancerName: "test",
            LoadBalancerArn: "test"
          }]
        });
      }
    }));
    stubs.push(sinon.stub(elbv2, "describeTargetHealth").returns({
      promise(){
        return Promise.resolve({
          TargetHealthDescriptions: [{
            Target: {
              Id: "1.2.3.4",
              Port: "80"
            },
            TargetHealth: {
              State: "healthy"
            }
          }]
        });
      }
    }));
    stubs.push(sinon.stub(elbv2, "describeTargetGroups").returns({
      promise(){
        return Promise.resolve({
          TargetGroups: [{
            TargetGroupName: "test",
            TargetGroupArn: "arn:test"
          }]
        });
      }
    }));
    stubs.push(sinon.stub(ec2, "describeInstances").returns({
      promise(){
        return Promise.resolve({
          Reservations: [{
            Instances: [{
              PrivateIpAddress: "1.2.3.4",
              InstanceId: "instance1",
              State: {
                Code: "running"
              }
            }]
          }]
        });
      }
    }));
    const deregisterSpy = sinon.stub(elbv2,"deregisterTargets").returns({
      promise(){
        return Promise.resolve({
        });
      }
    });
    stubs.push(deregisterSpy);
    cleanup(getApi({}))
    .then(function(){
      expect(deregisterSpy.called).to.equal(false);
      done();
    })
    .catch(function(e){
      done(e);
    });
  });

  it('should deregister instances when they are unhealthy, and no longer listed', (done) => {
    stubs.push(sinon.stub(elbv2, "describeLoadBalancers").returns({
      promise(){
        return Promise.resolve({
          LoadBalancers: [{
            LoadBalancerName: "test",
            LoadBalancerArn: "test"
          }]
        });
      }
    }));
    stubs.push(sinon.stub(elbv2, "describeTargetHealth").returns({
      promise(){
        return Promise.resolve({
          TargetHealthDescriptions: [{
            Target: {
              Id: "1.2.3.4",
              Port: "80"
            },
            TargetHealth: {
              State: "unhealthy"
            }
          }]
        });
      }
    }));
    stubs.push(sinon.stub(elbv2, "describeTargetGroups").returns({
      promise(){
        return Promise.resolve({
          TargetGroups: [{
            TargetGroupName: "test",
            TargetGroupArn: "arn:test"
          }]
        });
      }
    }));
    stubs.push(sinon.stub(ec2, "describeInstances").returns({
      promise(){
        return Promise.resolve({
          Reservations: []
        });
      }
    }));
    const deregisterSpy = sinon.stub(elbv2,"deregisterTargets").returns({
      promise(){
        return Promise.resolve({
        });
      }
    });
    stubs.push(deregisterSpy);
    cleanup(getApi({}))
    .then(function(){
      expect(deregisterSpy.called).to.equal(true);
      done();
    })
    .catch(function(e){
      done(e);
    });
  });


  it('should deregister instances when they are unhealthy, and listed as terminated', (done) => {
    stubs.push(sinon.stub(elbv2, "describeLoadBalancers").returns({
      promise(){
        return Promise.resolve({
          LoadBalancers: [{
            LoadBalancerName: "test",
            LoadBalancerArn: "test"
          }]
        });
      }
    }));
    stubs.push(sinon.stub(elbv2, "describeTargetHealth").returns({
      promise(){
        return Promise.resolve({
          TargetHealthDescriptions: [{
            Target: {
              Id: "1.2.3.4",
              Port: "80"
            },
            TargetHealth: {
              State: "unhealthy"
            }
          }]
        });
      }
    }));
    stubs.push(sinon.stub(elbv2, "describeTargetGroups").returns({
      promise(){
        return Promise.resolve({
          TargetGroups: [{
            TargetGroupName: "test",
            TargetGroupArn: "arn:test"
          }]
        });
      }
    }));
    stubs.push(sinon.stub(ec2, "describeInstances").returns({
      promise(){
        return Promise.resolve({
          Reservations: [{
            Instances: [{
              PrivateIpAddress: "1.2.3.4",
              instanceId: "instance1",
              State: {
                code: "terminated"
              }
            }]
          }]
        });
      }
    }));
    const deregisterSpy = sinon.stub(elbv2,"deregisterTargets").returns({
      promise(){
        return Promise.resolve({
        });
      }
    });
    stubs.push(deregisterSpy);
    cleanup(getApi({}))
    .then(function(){
      expect(deregisterSpy.called).to.equal(true);
      done();
    })
    .catch(function(e){
      done(e);
    });
  });


  it('should NOT deregister instances when they are unhealthy, and NOT listed as terminated', (done) => {
    stubs.push(sinon.stub(elbv2, "describeLoadBalancers").returns({
      promise(){
        return Promise.resolve({
          LoadBalancers: [{
            LoadBalancerName: "test",
            LoadBalancerArn: "test"
          }]
        });
      }
    }));
    stubs.push(sinon.stub(elbv2, "describeTargetHealth").returns({
      promise(){
        return Promise.resolve({
          TargetHealthDescriptions: [{
            Target: {
              Id: "1.2.3.4",
              Port: "80"
            },
            TargetHealth: {
              State: "unhealthy"
            }
          }]
        });
      }
    }));
    stubs.push(sinon.stub(elbv2, "describeTargetGroups").returns({
      promise(){
        return Promise.resolve({
          TargetGroups: [{
            TargetGroupName: "test",
            TargetGroupArn: "arn:test"
          }]
        });
      }
    }));
    stubs.push(sinon.stub(ec2, "describeInstances").returns({
      promise(){
        return Promise.resolve({
          Reservations: [{
            Instances: [{
              PrivateIpAddress: "1.2.3.4",
              InstanceId: "instance1",
              State: {
                code: "running"
              }
            }]
          }]
        });
      }
    }));
    const deregisterSpy = sinon.stub(elbv2,"deregisterTargets").returns({
      promise(){
        return Promise.resolve({
        });
      }
    });
    stubs.push(deregisterSpy);
    cleanup(getApi({}))
    .then(function(){
      expect(deregisterSpy.called).to.equal(false);
      done();
    })
    .catch(function(e){
      done(e);
    });
  });


  it('should deregister instances when they are unhealthy, and NOT listed as terminated, if unterminated flag is present', (done) => {
    stubs.push(sinon.stub(elbv2, "describeLoadBalancers").returns({
      promise(){
        return Promise.resolve({
          LoadBalancers: [{
            LoadBalancerName: "test",
            LoadBalancerArn: "test"
          }]
        });
      }
    }));
    stubs.push(sinon.stub(elbv2, "describeTargetHealth").returns({
      promise(){
        return Promise.resolve({
          TargetHealthDescriptions: [{
            Target: {
              Id: "1.2.3.4",
              Port: "80"
            },
            TargetHealth: {
              State: "unhealthy"
            }
          }]
        });
      }
    }));
    stubs.push(sinon.stub(elbv2, "describeTargetGroups").returns({
      promise(){
        return Promise.resolve({
          TargetGroups: [{
            TargetGroupName: "test",
            TargetGroupArn: "arn:test"
          }]
        });
      }
    }));
    stubs.push(sinon.stub(ec2, "describeInstances").returns({
      promise(){
        return Promise.resolve({
          Reservations: [{
            Instances: [{
              PrivateIpAddress: "1.2.3.4",
              InstanceId: "instance1",
              State: {
                code: "running"
              }
            }]
          }]
        });
      }
    }));
    const deregisterSpy = sinon.stub(elbv2,"deregisterTargets").returns({
      promise(){
        return Promise.resolve({
        });
      }
    });
    const terminateSpy = sinon.stub(ec2,"terminateInstances").returns({
      promise(){
        return Promise.resolve({
        });
      }
    });
    stubs.push(deregisterSpy);
    stubs.push(terminateSpy);
    cleanup(getApi({unterminated: true}))
    .then(function(){
      expect(deregisterSpy.called).to.equal(true);
      expect(terminateSpy.called).to.equal(true);
      done();
    })
    .catch(function(e){
      done(e);
    });
  });
});
