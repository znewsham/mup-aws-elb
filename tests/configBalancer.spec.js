import { expect } from 'chai';
import { configBalancer } from '../src/command-handlers';
import { ensureListeners } from '../src/elb/listeners';
import { ensureTargetGroups } from '../src/elb/target-groups';
import { ensureSecurityGroups} from '../src/elb/security-groups';
import { ensureBalancer, waitForBalancer } from '../src/elb/balancer';

import prepareConfig from '../src/prepareConfig';
import { ec2, elbv2 } from '../src/aws.js';
import configure from '../src/aws.js';
import sinon from 'sinon';
describe('configBalancer', () => {
  before(function(){
    configure({auth: {}, region: ""});
  });
  function getApi(options, balancerConfig = {}){
    return {
      getBasePath(){
        return "./";
      },
      resolvePath(){
        return "";
      },
      commandHistory:["elb.deploy"],
      getConfig(){
        return prepareConfig({
          app: {
            name: "test",
            docker: {
              prepareBundle: true
            },
            auth: {
              instanceUsername: "ubuntu",
            },
            deployment: {
              launchTemplate: {

              },
              balancer: balancerConfig
            }
          }
        });
      },
      getOptions(){
        return options;
      }
    }
  }
  let stubs = [];
  beforeEach(function(){
    stubs = [];
  });
  afterEach(function(){
    stubs.forEach(stub=>stub.restore());
  });

  describe("ensureBalancer", function(done){
    it("should return the load balancer when referenced by arn", function(done){
      stubs.push(sinon.stub(elbv2, "describeLoadBalancers").returns({
        promise(){
          return Promise.resolve({
            LoadBalancers: [{LoadBalancerArn: "test"}]
          });
        }
      }));
      const spy = sinon.stub(elbv2, "createLoadBalancer").returns({});
      stubs.push(spy);
      ensureBalancer(getApi({}, {arn: "test"}),{})
      .then(function(balancer){
        expect(balancer.LoadBalancerArn).to.equal("test");
        expect(spy.called).to.equal(false);
        done();
      })
      .catch(err=>done(err));
    });
    it("should return the load balancer when referenced by name", function(done){
      stubs.push(sinon.stub(elbv2, "describeLoadBalancers").returns({
        promise(){
          return Promise.resolve({
            LoadBalancers: [{LoadBalancerArn: "test", LoadBalancerName: "name"}]
          });
        }
      }));
      const spy = sinon.stub(elbv2, "createLoadBalancer").returns({});
      stubs.push(spy);
      ensureBalancer(getApi({}, {name: "name"}), {})
      .then(function(balancer){
        expect(balancer.LoadBalancerArn).to.equal("test");
        expect(spy.called).to.equal(false);
        done();
      })
      .catch(err=>done(err));
    });
    it("should throw an error when an arn referenced balancer does not exist", function(done){
      stubs.push(sinon.stub(elbv2, "describeLoadBalancers").returns({
        promise(){
          return Promise.resolve({
            LoadBalancers: []
          });
        }
      }));
      const spy = sinon.stub(elbv2, "createLoadBalancer").returns({});
      stubs.push(spy);
      ensureBalancer(getApi({}, {arn: "test"}), {})
      .then(function(balancer){
        done(new Error("Should have errored by now"));
      })
      .catch(function(err){
        expect(err.message).to.equal("The specified balancer does not exist");
        done();
      })
      .catch(err=>done(err));
    });
    it("should create a balancer when it does not exist", function(done){
      stubs.push(sinon.stub(elbv2, "describeLoadBalancers").returns({
        promise(){
          return Promise.resolve({
            LoadBalancers: []
          });
        }
      }));
      const spy = sinon.stub(elbv2, "createLoadBalancer").returns({
        promise(){
          return Promise.resolve({
            LoadBalancers: [{LoadBalancerArn: "test"}]
          })
        }
      });
      stubs.push(spy);
      ensureBalancer(getApi({}, {name: "test"}), {})
      .then(function(balancer){
        expect(balancer.LoadBalancerArn).to.equal("test");
        expect(spy.called).to.equal(true);
        done();
      })
      .catch(err=>done(err));
    });
  });

  describe("waitForBalancer", function(){
    it("should return when the load balancer is ready", function(done){
      let state = "active";
      stubs.push(sinon.stub(elbv2,"describeLoadBalancers").returns({
        promise(){
          return Promise.resolve({
            LoadBalancers: [{LoadBalancerArn: "1", State: {Code: state}}]
          });
        }
      }));
      waitForBalancer("1", 1, 500)
      .then(function(groups){
        done();
      })
      .catch(function(err){
        done(err);
      });
    });
    it("should throw an error when the load balancer state is neither provisioning or active", function(done){
      let state = "test";
      stubs.push(sinon.stub(elbv2,"describeLoadBalancers").returns({
        promise(){
          return Promise.resolve({
            LoadBalancers: [{LoadBalancerArn: "1", State: {Code: state}}]
          });
        }
      }));
      waitForBalancer("1", 1, 500)
      .then(function(groups){
        done(new Error("Should have caught an error by now"));
      })
      .catch(function(err){
        expect(err.message).to.equal("Load balancer state is test");
        done();
      })
      .catch(function(err){
        done(err);
      });
    });
    it("should throw an error when the load balancer state does not change after timeout", function(done){
      let state = "provisioning";
      stubs.push(sinon.stub(elbv2,"describeLoadBalancers").returns({
        promise(){
          return Promise.resolve({
            LoadBalancers: [{LoadBalancerArn: "1", State: {Code: state}}]
          });
        }
      }));
      waitForBalancer("1", 1, 500)
      .then(function(groups){
        done(new Error("Should have caught an error by now"));
      })
      .catch(function(err){
        expect(err.message).to.equal("Load balancer never left provisioning");
        done();
      })
      .catch(function(err){
        done(err);
      });
    });
  });
  describe("ensureListeners", function(){
    it("should return the existing listeners when identified by listenerArn, and they exist", function(done){
      stubs.push(sinon.stub(elbv2,"describeListeners").returns({
        promise(){
          return Promise.resolve({
            Listeners: [{ListenerArn: "1"}]
          });
        }
      }));
      const spy = sinon.stub(elbv2, "createListener").returns({});
      stubs.push(spy);
      ensureListeners("", [{TargetGroupArn: "1"}],[{listenerArn: "1"}])
      .then(function(groups){
        expect(groups.length).to.equal(1);
        expect(groups[0].ListenerArn).to.equal("1");
        expect(spy.called).to.equal(false);
        done();
      })
      .catch(function(err){
        done(err);
      });
    });
    it("should return the existing listeners when identified by Port, and they exist", function(done){
      stubs.push(sinon.stub(elbv2,"describeListeners").returns({
        promise(){
          return Promise.resolve({
            Listeners: [{Port: 80, ListenerArn: "1"}]
          });
        }
      }));
      const spy = sinon.stub(elbv2, "createListener").returns({});
      stubs.push(spy);
      ensureListeners("", [{TargetGroupArn: "1"}],[{port: 80}])
      .then(function(groups){
        expect(groups.length).to.equal(1);
        expect(groups[0].ListenerArn).to.equal("1");
        expect(spy.called).to.equal(false);
        done();
      })
      .catch(function(err){
        done(err);
      });
    });

    it("should return create thelisteners when they dont exist", function(done){
      stubs.push(sinon.stub(elbv2,"describeListeners").returns({
        promise(){
          return Promise.resolve({
            Listeners: []
          });
        }
      }));
      const spy = sinon.stub(elbv2, "createListener").returns({
        promise(){
          return Promise.resolve({
            Listeners: [{ListenerArn: "1"}]
          });
        }
      });
      stubs.push(spy);
      ensureListeners("", [{TargetGroupArn: "test"}],[{port: 80}])
      .then(function(groups){
        expect(groups.length).to.equal(1);
        expect(groups[0].ListenerArn).to.equal("1");
        expect(spy.called).to.equal(true);
        done();
      })
      .catch(function(err){
        done(err);
      });
    });
  });

  describe("ensureTargetGroups", function(){
    it("should return the existing targetGroups when identified by targetGroupArn, and they exist", function(done){
      stubs.push(sinon.stub(elbv2,"describeTargetGroups").returns({
        promise(){
          return Promise.resolve({
            TargetGroups: [{TargetGroupArn: "1"}]
          });
        }
      }));
      const spy = sinon.stub(elbv2, "createTargetGroup").returns({});
      stubs.push(spy);
      ensureTargetGroups("", [{targetGroupArn: "1"}])
      .then(function(groups){
        expect(groups.length).to.equal(1);
        expect(groups[0].TargetGroupArn).to.equal("1");
        expect(spy.called).to.equal(false);
        done();
      })
      .catch(function(err){
        done(err);
      });
    });

    it("should return the existing targetGroups when identified by targetGroupName, and they exist", function(done){
      stubs.push(sinon.stub(elbv2,"describeTargetGroups").returns({
        promise(){
          return Promise.resolve({
            TargetGroups: [{TargetGroupArn: "1", TargetGroupName: "test"}]
          });
        }
      }));
      const spy = sinon.stub(elbv2, "createTargetGroup").returns({});
      stubs.push(spy);
      ensureTargetGroups("", [{targetGroupName: "test"}])
      .then(function(groups){
        expect(groups.length).to.equal(1);
        expect(groups[0].TargetGroupArn).to.equal("1");
        expect(spy.called).to.equal(false);
        done();
      })
      .catch(function(err){
        done(err);
      });
    });

    it("should return create the target groups when they dont exist", function(done){
      stubs.push(sinon.stub(elbv2,"describeTargetGroups").returns({
        promise(){
          return Promise.resolve({
            TargetGroups: []
          });
        }
      }));
      const spy = sinon.stub(elbv2, "createTargetGroup").returns({
        promise(){
          return Promise.resolve({
            TargetGroups: [{TargetGroupArn: "1"}]
          });
        }
      });
      stubs.push(sinon.stub(elbv2,"modifyTargetGroupAttributes").returns({
        promise(){
          return Promise.resolve({
            TargetGroups: []
          });
        }
      }));
      stubs.push(spy);
      ensureTargetGroups("", [{targetGroupName: "test", attributes: []}])
      .then(function(groups){
        expect(groups.length).to.equal(1);
        expect(groups[0].TargetGroupArn).to.equal("1");
        expect(spy.called).to.equal(true);
        done();
      })
      .catch(function(err){
        done(err);
      });
    });
  });
  describe("ensureSecurityGroups", function(){
    it("should return the groupIds when the array contains id's", function(done){
      let groups = [{groupId: "1"},{groupId: "2"}];
      ensureSecurityGroups("",groups, getApi({}))
      .then(function(res){
        expect(res).to.deep.equal(groups.map(g=>g.groupId));
        done();
      })
      .catch(e=>done(e));
    });
    it("should fetch groupIds for groups with names", function(done){
      let groups = [{groupName: "1"},{groupName: "2"}];
      let count = 0;
      stubs.push(sinon.stub(ec2,"describeSecurityGroups").returns({
        promise(){
          count++;
          if(count == 1){
            return Promise.resolve({
              SecurityGroups: [{GroupName: "1", GroupId: "1"}]
            });
          }
          else {
            return Promise.resolve({
              SecurityGroups: [{GroupName: "2", GroupId: "2"}]
            });
          }
        }
      }));
      ensureSecurityGroups("",groups, getApi({}))
      .then(function(res){
        expect(res).to.deep.equal(groups.map(g=>g.groupName));
        done();
      })
      .catch(e=>done(e));
    });
    it("should create missing groups", function(done){
      let groups = [{groupName: "1"}];
      let created = false;
      stubs.push(sinon.stub(ec2,"describeSecurityGroups").returns({
        promise(){
          if(!created){
            return Promise.reject({

            });
          }
          else {
            return Promise.resolve({
              GroupId: [{GroupName: "1", GroupId: "1"}]
            });
          }
        }
      }));
      stubs.push(sinon.stub(ec2,"createSecurityGroup").returns({
        promise(){
          created = true
          return Promise.resolve(
            {GroupId: "1"}
          );
        }
      }));
      stubs.push(sinon.stub(ec2,"authorizeSecurityGroupIngress").returns({
        promise(){
          return Promise.resolve({

          });
        }
      }));
      ensureSecurityGroups("",groups, getApi({}))
      .then(function(res){

        expect(res).to.deep.equal(groups.map(g=>g.groupName));
        done();
      })
      .catch(e=>done(e));
    });
  });

});
