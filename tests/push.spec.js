import { expect } from 'chai';
import { beforeEachServerDeploy } from '../src/command-handlers';
import prepareConfig from '../src/prepareConfig';
import { ec2, elbv2 } from '../src/aws.js';
import configure from '../src/aws.js';
import sinon from 'sinon';
describe('beforeEachServerDeploy', () => {
  before(function(){
    configure({auth: {}, region: ""});
  });
  function getApi(options){
    return {
      getBasePath(){
        return "./";
      },
      resolvePath(){
        return "";
      },
      commandHistory:["elb.deploy"],
      getConfig(){
        return prepareConfig({
          app: {
            name: "test",
            docker: {
              prepareBundle: true
            },
            auth: {
              instanceUsername: "ubuntu",
            },
            deployment: {
              launchTemplate: {

              },
              balancer: {

              }
            }
          }
        });
      },
      getOptions(){
        return options;
      }
    }
  }
  let stubs = [];
  beforeEach(function(){
    stubs = [];
  });
  afterEach(function(){
    stubs.forEach(stub=>stub.restore());
  });
  it("should refuse to deploy when there are < 2 healthy hosts", function(done){
    const deregisterSpy = sinon.stub(elbv2,"deregisterTargets").returns({
      promise(){
        return Promise.resolve({
        });
      }
    });
    stubs.push(deregisterSpy);

    beforeEachServerDeploy(getApi({}), "test",{_host: "1.2.3.4"}, [{Id: "1.2.3.4", SessionIp: "1.2.3.4"}],[{Target: {Id: "1.2.3.4"}, TargetHealth:{State: "healthy"}}])
    .then(function(){
      expect(deregisterSpy.called).to.equal(false);
      done(new Error("We shouldn't have gotten here"));
    })
    .catch(function(e){
      try {
        expect(e.message).to.equal("Deploying to this server would result in down time - use --downtime-acceptable if that is acceptable");
        done();
      }
      catch(e1){
        e1.stack = e.stack;
        return Promise.reject(e1);
      }
    })
    .catch(function(e){
      done(e);
    });
  });
  it("should deploy when there are < 2 healthy hosts, and --downtime-acceptable is true", function(done){
    const deregisterSpy = sinon.stub(elbv2,"deregisterTargets").returns({
      promise(){
        return Promise.resolve({
        });
      }
    });
    stubs.push(deregisterSpy);

    beforeEachServerDeploy(getApi({"downtime-acceptable":true}), "test",{_host: "1.2.3.4"}, [{Id: "1.2.3.4", SessionIp: "1.2.3.4"}],[{Target: {Id: "1.2.3.4"}, TargetHealth:{State: "healthy"}}])
    .then(function(){
      expect(deregisterSpy.called).to.equal(true);
      done();
    })
    .catch(function(e){
      done(e);
    });
  });

  it("should deploy when there are >= 2 healthy hosts", function(done){
    const deregisterSpy = sinon.stub(elbv2,"deregisterTargets").returns({
      promise(){
        return Promise.resolve({
        });
      }
    });
    stubs.push(deregisterSpy);

    beforeEachServerDeploy(getApi({}), "test",{_host: "1.2.3.4"}, [{Id: "1.2.3.5", SessionIp: "1.2.3.5"}, {Id: "1.2.3.4", SessionIp: "1.2.3.4"}],[{Target: {Id: "1.2.3.4"}, TargetHealth:{State: "healthy"}}, {Target: {Id: "1.2.3.5"}, TargetHealth:{State: "healthy"}}])
    .then(function(){
      expect(deregisterSpy.called).to.equal(true);
      done();
    })
    .catch(function(e){
      done(e);
    });
  });
});
