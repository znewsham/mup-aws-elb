import { elbv2 } from '../aws';
import { deepUcFirst } from '../utils';

export function getTargetGroups(params) {
  return elbv2.describeTargetGroups(params)
  .promise()
  .then(({ TargetGroups }) => TargetGroups);
}

export async function ensureTargetGroups(balancerArn, targetGroups, vpcId) {
  targetGroups.forEach((tg) => {
    if (!tg.vpcId) {
      tg.vpcId = vpcId;
    }
  });
  const existingGroups = await elbv2.describeTargetGroups({ LoadBalancerArn: balancerArn })
  .promise()
  .then(({ TargetGroups }) => TargetGroups);
  return Promise.all(targetGroups.map((tg) => {
    if (tg.targetGroupArn) {
      const group = existingGroups.find(g => g.TargetGroupArn === tg.targetGroupArn);
      if (group) {
        return Promise.resolve(group);
      }
    }
    const group = existingGroups.find(g => g.TargetGroupName === tg.name);
    if (group) {
      return Promise.resolve(group);
    }
    const targetGroupAttributes = tg.attributes;
    delete tg.attributes;
    return elbv2.createTargetGroup(deepUcFirst(tg))
    .promise()
    .then(({ TargetGroups }) => TargetGroups[0])
    // NOTE: inline arrow is confusing here, messes with indentation
    // eslint-disable-next-line arrow-body-style
    .then((targetGroup) => {
      return elbv2.modifyTargetGroupAttributes({
        TargetGroupArn: targetGroup.TargetGroupArn,
        Attributes: targetGroupAttributes.map(a => deepUcFirst(a))
      })
      .promise()
      .then(() => targetGroup);
    });
  }));
}
