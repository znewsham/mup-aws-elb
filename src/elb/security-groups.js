import { ec2 } from '../aws';

export async function createSecurityGroups(vpcId, groupsToCreate, allGroups, port) {
  return Promise.all(groupsToCreate.map(group => ec2.createSecurityGroup({
    GroupName: group.groupName,
    Description: group.description || group.groupName,
    VpcId: vpcId
  })
  .promise()
  .then(({ GroupId }) => {
    group.groupId = GroupId;
  })))
  .then(() => {
    let additionalNames = [];
    groupsToCreate.forEach((g) => {
      if (g.inboundGroupNames) {
        additionalNames = additionalNames.concat(g.inboundGroupNames);
      }
      if (g.inbound) {
        g.inbound.forEach((i) => {
          if (i.groupNames) {
            additionalNames = additionalNames.concat(i.groupNames);
          }
        });
      }
    });
    return Promise.all(additionalNames.map(aName => ec2.describeSecurityGroups({
      GroupNames: [aName]
    })
    .promise()
    .then(({ SecurityGroups }) => SecurityGroups[0])))
    .then((newGroups) => {
      newGroups.forEach((g) => {
        g.groupId = g.GroupId;
        g.groupName = g.GroupName;
        allGroups.push(g);
      });
    });
  })
  .then(() => Promise.all(groupsToCreate.map((group) => {
    if (!group.inbound) {
      group.inbound = [{
        port
      }];
    }
    if (group.inbound.length) {
      const params = {
        GroupId: group.groupId,
        IpPermissions: group.inbound.map((inbound) => {
          const param = {
            IpProtocol: 'tcp',
            FromPort: inbound.port,
            ToPort: inbound.port
          };
          if (!inbound.groupNames && group.inboundGroupNames) {
            inbound.groupNames = group.inboundGroupNames;
          }
          if (inbound.groupNames) {
            param.UserIdGroupPairs = inbound.groupNames.map(aName => ({
              GroupId: (groupsToCreate.find(g => g.groupName === aName) || allGroups.find(g => g.groupName === aName)).groupId
            }));
          }
          else {
            param.IpRanges = [{ CidrIp: '0.0.0.0/0' }];
          }
          return param;
        })
      };
      return ec2.authorizeSecurityGroupIngress(params).promise()
      .then(() => group);
    }
    return Promise.resolve();
  })));
}

export async function ensureSecurityGroups(vpcId, securityGroups, port) {
  if (securityGroups.some(g => !g.groupId)) {
    const groupNames = securityGroups.filter(sg => !sg.groupId).map(g => g.groupName);
    /* securityGroups.forEach((g) => {
      if (g.inboundGroupNames) {
        groupNames = groupNames.concat(g.inboundGroupNames);
      }
      else if (g.inbound) {
        g.inbound.forEach((i) => {
          if (i.groupNames) {
            groupNames = groupNames.concat(i.groupNames);
          }
        });
      }
    }); */
    const groups = (await Promise.all(groupNames.map(g => ec2.describeSecurityGroups({
      GroupNames: [g]
    })
    .promise()
    .then(({ SecurityGroups }) => SecurityGroups[0])
    .catch(() => null))))
    .filter(g => g !== null);
    groups.forEach((g) => {
      securityGroups.find(g1 => g1.groupName === g.GroupName).groupId = g.GroupId;
    });
    let missingGroups = securityGroups.filter(g => !g.groupId).filter(g => !groups.find(g1 => g1.GroupName === g.groupName));
    if (missingGroups.length) {
      missingGroups = await createSecurityGroups(vpcId, missingGroups, securityGroups, port);
      missingGroups.forEach((g) => {
        securityGroups.find(g1 => g1.groupName === g.groupName).groupId = g.groupId;
      });
    }
    const obj = {};
    groups.forEach((g) => {
      obj[g.GroupId] = true;
    });
    securityGroups.forEach((g) => {
      obj[g.groupId] = true;
    });
    return Object.keys(obj);
  }
  return securityGroups.map(g => g.groupId);
}

export function getSecurityGroups(params) {
  return ec2.describeSecurityGroups(params)
  .promise()
  .then(({ SecurityGroups }) => SecurityGroups);
}
