import { elbv2 } from '../aws';

export function getBalancer(api) {
  const config = api.getConfig().app.deployment.balancer;
  const params = {};
  if (config.arn) {
    params.LoadBalancerArns = [config.arn];
  }
  return elbv2.describeLoadBalancers(params)
  .promise()
  .then(({ LoadBalancers }) => LoadBalancers.find((lb) => {
    if (config.arn) {
      return lb.LoadBalancerArn === config.arn;
    }
    return lb.LoadBalancerName === config.name;
  }));
}

export function waitForBalancer(balancerArn, waitOnCreateTimeoutSeconds, intervalInMillis = 10000) {
  return new Promise((resolve, reject) => {
    console.log(`Waiting upto ${waitOnCreateTimeoutSeconds} seconds for load balancer to go active`);
    let waitInterval;
    const watchdogTimeout = setTimeout(() => {
      clearInterval(waitInterval);
      clearTimeout(watchdogTimeout);
      reject(new Error('Load balancer never left provisioning'));
    }, waitOnCreateTimeoutSeconds * 1000);
    waitInterval = setInterval(() => {
      elbv2.describeLoadBalancers({ LoadBalancerArns: [balancerArn] })
      .promise()
      .then(({ LoadBalancers }) => {
        if (LoadBalancers[0].State.Code !== 'provisioning') {
          clearInterval(waitInterval);
          clearTimeout(watchdogTimeout);
          if (LoadBalancers[0].State.Code !== 'active') {
            reject(new Error(`Load balancer state is ${LoadBalancers[0].State.Code}`));
          }
          else {
            resolve();
          }
        }
      });
    }, intervalInMillis);
  });
}

export async function ensureBalancer(api, params) {
  const balancer = await getBalancer(api);
  if (!balancer && api.getConfig().app.deployment.balancer.arn) {
    throw new Error('The specified balancer does not exist');
  }
  else if (!balancer) {
    return elbv2.createLoadBalancer(params).promise().then(({ LoadBalancers }) => LoadBalancers[0]);
  }
  else {
    return balancer;
  }
}
