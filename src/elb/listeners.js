import { elbv2 } from '../aws';
import { deepUcFirst } from '../utils';

export function getListeners(params) {
  return elbv2.describeListeners(params)
  .promise()
  .then(({ Listeners }) => Listeners);
}
export function getRules(params) {
  return elbv2.describeRules(params)
  .promise()
  .then(({ Rules }) => Rules);
}

export async function ensureRules(listenerArn, newRules, targetGroupArn) {
  const existingRules = (await getRules({ ListenerArn: listenerArn })).filter(r => r.Priority !== 'default');
  const missingRules = newRules.filter((newRule) => {
    const newRuleIdentfier = newRule.conditions.map(c => c.field + c.value)[0];
    return !existingRules.find(existingRule => newRuleIdentfier === existingRule.Conditions.map(c => c.Field + c.Values[0])[0]);
  });
  return Promise.all(missingRules.map(missingRule => elbv2.createRule({
    ListenerArn: listenerArn,
    Priority: missingRule.priority,
    Actions: [{
      Type: 'forward',
      TargetGroupArn: targetGroupArn
    }],
    Conditions: missingRule.conditions.map(condition => ({ Field: condition.field, Values: [condition.value] }))
  }).promise()));
}

export async function ensureListeners(balancerArn, targetGroupArn, listeners) {
  const existingListeners = await elbv2.describeListeners({ LoadBalancerArn: balancerArn })
  .promise()
  .then(({ Listeners }) => Listeners);
  return Promise.all(listeners.map((aListener) => {
    const { rules } = aListener;
    delete aListener.rules;
    const listener = deepUcFirst(aListener);
    return Promise.resolve()
    .then(() => {
      if (listener.ListenerArn) {
        const existingListener = existingListeners.find(l => l.ListenerArn === listener.ListenerArn);
        if (existingListener) {
          return Promise.resolve(existingListener);
        }
      }
      else if (listener.Port) {
        const existingListener = existingListeners.find(l => l.Port === listener.Port);
        if (existingListener) {
          return Promise.resolve(existingListener);
        }
      }
      listener.LoadBalancerArn = balancerArn;
      if (!listener.DefaultActions) {
        listener.DefaultActions = [{
          Type: 'forward',
          TargetGroupArn: targetGroupArn
        }];
      }
      delete listener.TargetGroupName;
      return elbv2.createListener(listener)
      .promise()
      .then(async ({ Listeners }) => Listeners[0]);
    })
    .then(createdListener => ensureRules(createdListener.ListenerArn, rules, targetGroupArn).then(() => listener));
  }));
}
