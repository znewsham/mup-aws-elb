import * as commandHandlers from './command-handlers';

import configure from './aws';

commandHandlers.balancer = commandHandlers.balancerCommand;
delete commandHandlers.balancerCommand;
let prepared = false;

function prepare(commandHandler) {
  return function handler(api) {
    if (!prepared) {
      configure(api.getConfig().app);
      prepared = true;
    }

    return commandHandler(api);
  };
}

export const pushToNewServers = {
  description: 'pushToNewServers',
  builder(subYargs) {
    return subYargs.option('public-addresses', {
      description: 'Use public addresses',
      boolean: true
    });
  },
  handler: prepare(commandHandlers.pushToNewServers)
};

export const servers = {
  description: 'List the servers currently in use',
  builder(subYargs) {
    return subYargs.option('public-addresses', {
      description: 'Use public addresses',
      boolean: true
    })
    .option('target-group-arn', {
      description: 'The ARN of the target group to list the instances'
    })
    .option('target-group-name', {
      description: 'The ARN of the target group to list the instances'
    });
  },
  handler: prepare(commandHandlers.servers)
};

export const cleanup = {
  description: 'Remove unhealthy, terminated instances from the target group',
  builder(subYargs) {
    return subYargs.option('unterminated', {
      description: 'Remove (and terminate) unhealthy instances that are still running',
      boolean: false
    })
    .option('target-group-arn', {
      description: 'The ARN of the target group to cleanup'
    })
    .option('target-group-name', {
      description: 'The ARN of the target group to cleanup'
    });
  },
  handler: prepare(commandHandlers.cleanup)
};

export const ensureBaseInstances = {
  description: 'list the servers',
  builder(subYargs) {
    return subYargs.option('public-addresses', {
      description: 'Use public addresses',
      boolean: true
    })
    .option('cached-build', {
      description: 'Use build from previous deploy',
      boolean: true
    })
    .option('target-group-arn', {
      description: 'The ARN of the target group to register the new instances'
    })
    .option('target-group-name', {
      description: 'The ARN of the target group to register the new instances'
    });
  },
  handler: prepare(commandHandlers.ensureBaseInstances)
};

export const spinupInstances = {
  description: 'spinup new servers',
  builder(subYargs) {
    return subYargs.option('public-addresses', {
      description: 'Use public addresses',
      boolean: true
    })
    .option('cached-build', {
      description: 'Use build from previous deploy',
      boolean: true
    })
    .option('instances', {
      description: 'The number of instances to start'
    })
    .option('target-group-arn', {
      description: 'The ARN of the target group to register the new instances'
    })
    .option('target-group-name', {
      description: 'The ARN of the target group to register the new instances'
    });
  },
  handler: prepare(commandHandlers.spinupInstances)
};

export const deploy = {
  description: 'Deploy meteor apps',
  builder(subYargs) {
    return subYargs.option('cached-build', {
      description: 'Use build from previous deploy',
      boolean: true
    })
    .option('public-addresses', {
      description: 'Use public addresses',
      boolean: true
    })
    .option('downtime-acceptable', {
      description: 'Use if your application gets in a bad state (i.e., 0 or 1 healthy servers)',
      boolean: true
    });
  },
  handler: prepare(commandHandlers.deploy)
};

export const push = {
  description: 'Push meteor apps',
  builder(subYargs) {
    return subYargs;
  },
  handler: prepare(commandHandlers.push)
};

export const balancer = {
  description: 'Get details of the load balancer',
  handler: prepare(commandHandlers.balancer)
};
export const setup = {
  description: 'Setup the servers',
  handler: prepare(commandHandlers.setup)
};

export const configBalancer = {
  description: 'Configure the balancer',
  builder(subYargs) {
    return subYargs.option('public-addresses', {
      description: 'Use public addresses',
      boolean: true
    });
  },
  handler: prepare(commandHandlers.configBalancer)
};

export const logs = {
  description: 'Show app\'s logs. Supports options from docker logs',
  builder(yargs) {
    return yargs
    .strict(false)
    .option('tail', {
      description: 'Number of lines to show from the end of the logs',
      number: true
    })
    .option('follow', {
      description: 'Follow log output',
      alias: 'f',
      boolean: true
    })
    .option('timestamps', {
      description: 'Show timestamps',
      alias: 't',
      boolean: true
    })
    .option('since', {
      description: 'Since time',
      scoreString: true
    });
  },
  handler: prepare(commandHandlers.logs)
};
