import { promisify } from 'bluebird';

import './nodemiral';
import * as _commands from './commands';
import { getTargetGroupArn, getServers, registerTargets } from './command-handlers';
import { getBuildOptions, shouldRebuild, getSessions, archiveApp } from './utils';
import prepareConfig from './prepareConfig';
import validateElb from './validate';
import configure from './aws';

function isELBApp(api) {
  const config = api.getConfig();

  if (config.app && config.app.type === 'aws-elb') {
    return true;
  }

  return false;
}

async function forceSessions(api) {
  if (Object.keys(api.getSessions).indexOf('skip') !== -1) {
    return;
  }
  configure(api.getConfig().app);
  api.oldGetSessions = api.getSessions;
  const newServerSessions = getSessions(api, await getServers(api, await getTargetGroupArn(api), true));
  api.getSessions = function getSessions1() {
    return newServerSessions;
  };
}
function restoreSessions(api) {
  if (api.getSessions.skip) {
    return;
  }
  api.getSessions = api.oldGetSessions;
}
module.exports = {
  // (optional) Name of plugin. Defaults to name of package
  name: 'elb',

  // Description of top-level command, shown in `mup help`
  description: 'Deploy to instances attached to an ELB',
  commands: _commands,
  hooks: {
    'pre.proxy.reconfigShared': forceSessions,
    'post.proxy.reconfigShared': restoreSessions,
    'pre.proxy.setup': forceSessions,
    'post.proxy.setup': restoreSessions,
    'pre.setup': forceSessions,
    'post.setup': restoreSessions,
    'pre.reconfig': forceSessions,
    'post.reconfig': restoreSessions,
    'pre.meteor.stop': forceSessions,
    'post.meteor.stop': restoreSessions,
    'pre.meteor.envconfig': forceSessions,
    'post.meteor.envconfig': restoreSessions,
    'pre.meteor.start': forceSessions,
    'post.meteor.start': restoreSessions,
    // NOTE: after every build, check to see if we should be rebuilding the archive, if so - do so.
    'post.meteor.build': (api) => {
      if (isELBApp(api)) {
        const buildOptions = getBuildOptions(api);
        if (shouldRebuild(api)) {
          return promisify(archiveApp)(buildOptions.buildLocation, api);
        }
      }
    },
    'post.default.deploy': (api) => {
      if (isELBApp) {
        return api.runCommand('elb.deploy');
      }
    },
    // NOTE: before we deploy, we build, ensure the balancer is created and ensure we have at least N instances running.
    // TODO: in the case that we have < N instances running
    //       we actually deploy twice, might we worth just spinning them up and not deploying
    'pre.elb.deploy': (api) => {
      if (isELBApp(api)) {
        return api.runCommand('meteor.build')
        .then(() => api.runCommand('elb.configBalancer'))
        .then(() => api.runCommand('elb.ensureBaseInstances'));
      }
    },
    'post.elb.spinupInstances': (api) => {
      if (isELBApp(api)) {
        api.oldGetSessions = api.getSessions;
        if (!api.newInstances || api.newInstances.length === 0) {
          return;
        }
        const newServerSessions = getSessions(api, api.newInstances.map(i => ({
          SessionIp: api.getOptions()['public-addresses'] ? i.PublicIpAddress : i.PrivateIpAddress
        })));
        api.getSessions = function getSessions1() {
          return newServerSessions;
        };
        return api.runCommand('docker.setup')
        .then(() => api.runCommand('meteor.setup'))
        .then(() => {
          // NOTE: we only push to the new servers if we're calling spinup direct.
          //       if we're calling as part of a deployment, we'll deploy next
          if (!api.commandHistory.find(({ name }) => name === 'elb.deploy')) {
            return api.runCommand('meteor.build')
            .then(() => api.runCommand('elb.pushToNewServers'));
          }
          return registerTargets(api, api.newInstances.map(i => i.PrivateIpAddress));
        })
        .then(() => {
          api.getSessions = api.oldGetSessions;
        });
      }
    }
  },
  validate: {
    app: validateElb
  },
  // (optional) Called right after the config is loaded
  prepareConfig,
  // (optional) Called by api.scrubConfig(),
  // which is used by `mup validate --show --scrub`
  scrubConfig(config) {
    // Replace any senstive information in the config,
    // such as passwords and ip addresses.

    return config;
  }
};
