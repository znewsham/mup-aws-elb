import AWS from 'aws-sdk';

/* eslint-disable import/no-mutable-exports */
export let elbv2 = {};
export let ec2 = {};
/* eslint-enable import/no-mutable-exports */
let configured = false;
export default function configure({ auth, region }) {
  const options = {
    accessKeyId: auth.id,
    secretAccessKey: auth.secret,
    region: region || 'us-east-1'
  };

  AWS.config.update(options);
  if (!configured) {
    configured = true;
    elbv2 = new AWS.ELBv2({});
    ec2 = new AWS.EC2({ apiVersion: '2016-11-15' });
  }
}
