import fs from 'fs';
import tar from 'tar';
import os from 'os';
import uuid from 'uuid';
import nodemiral from 'nodemiral';
import random from 'random-seed';

const asyncFunc = require('async');

export function deepUcFirst(object, clone = {}) {
  Object.keys(object).forEach((key) => {
    if (key.charAt(0).toUpperCase() !== key) {
      if (Array.isArray(object[key])) {
        clone[key.charAt(0).toUpperCase() + key.substr(1)] = object[key].map((val) => {
          if (typeof val === 'object') {
            return deepUcFirst(val);
          }
          return val;
        });
      }
      else if (typeof object[key] === 'object') {
        clone[key.charAt(0).toUpperCase() + key.substr(1)] = deepUcFirst(object[key]);
      }
      else {
        clone[key.charAt(0).toUpperCase() + key.substr(1)] = object[key];
      }
    }
  });
  return clone;
}
export function archiveApp(buildLocation, api, cb) {
  const bundlePath = api.resolvePath(buildLocation, 'bundle.tar.gz');

  console.log('starting archive');
  tar.c({
    file: bundlePath,
    onwarn(message, data) {
      console.log(message, data);
    },
    cwd: buildLocation,
    portable: true,
    gzip: {
      level: 9
    }
  }, ['bundle'], (err) => {
    console.log('archive finished');
    if (err) {
      console.log('=> Archiving failed: ', err.message);
    }
    cb(err);
  });
}

function tmpBuildPath(appPath, api) {
  const rand = random.create(appPath);
  const uuidNumbers = [];
  for (let i = 0; i < 16; i++) {
    uuidNumbers.push(rand(255));
  }

  return api.resolvePath(
    os.tmpdir(),
    `mup-meteor-${uuid.v4({ random: uuidNumbers })}`
  );
}

export function getBuildOptions(api) {
  const config = api.getConfig().app;
  const appPath = api.resolvePath(api.getBasePath(), config.path);

  const buildOptions = config.buildOptions || {};
  buildOptions.buildLocation =
    buildOptions.buildLocation || tmpBuildPath(appPath, api);

  return buildOptions;
}

export function shouldRebuild(api) {
  let rebuild = true;
  const { buildLocation } = getBuildOptions(api);
  const bundlePath = api.resolvePath(buildLocation, 'bundle.tar.gz');

  if (api.getOptions()['cached-build']) {
    const buildCached = fs.existsSync(bundlePath);

    // If build is not cached, rebuild is true
    // even though the --cached-build flag was used
    if (buildCached) {
      rebuild = false;
    }
  }

  return rebuild;
}

export function prepareBundleSupported(dockerConfig) {
  const supportedImages = ['abernix/meteord', 'zodern/meteor'];

  if ('prepareBundle' in dockerConfig) {
    return dockerConfig.prepareBundle;
  }

  return supportedImages.find(supportedImage => dockerConfig.image.indexOf(supportedImage) === 0) || false;
}


export function getSessions(api, servers) {
  const config = api.getConfig();
  const res = servers.map((server) => {
    const auth = {
      username: config.app.auth.instanceUsername
    };
    const opts = {
      ssh: {}
    };

    const sshAgent = process.env.SSH_AUTH_SOCK;

    opts.ssh.keepaliveInterval = 1000 * 28;
    opts.ssh.keepaliveCountMax = 12;
    const info = {
      pem: config.app.auth.instancePem,
      password: config.app.auth.instancePassword
    };
    if (info.pem) {
      try {
        auth.pem = fs.readFileSync(api.resolvePath(info.pem), 'utf8');
      }
      catch (e) {
        console.error(`Unable to load pem at "${api.resolvePath(info.pem)}"`);
        console.log(e);
        if (e.code !== 'ENOENT') {
          console.log(e);
        }
        process.exit(1);
      }
    }
    else if (info.password) {
      auth.password = info.password;
    }
    else if (sshAgent && fs.existsSync(sshAgent)) {
      opts.ssh.agent = sshAgent;
    }
    else {
      console.error(
        "error: server %s doesn't have password, ssh-agent or pem",
        server.SessionIp
      );
      process.exit(1);
    }
    return nodemiral.session(server.SessionIp, auth, opts);
  });
  return res;
}

export async function setupList(list) {
  list.run = async function run(sessions, options, callback) {
    const self = this;

    if (!sessions) {
      throw new Error('First parameter should be either a session or a list of sessions');
    }
    else if (!(sessions instanceof Array)) {
      sessions = [sessions];
    }

    if ((typeof options) === 'function') {
      callback = options;
      options = {};
    }
    options = options || {};
    const summaryMap = {};

    // eslint-disable-next-line no-underscore-dangle
    self.log('info', `\nStarted TaskList: ${this._name}`);

    async function iterator(session, cb) {
      if (self.beforeExecute) {
        await self.beforeExecute(session);
      }
      async function cb2(err) {
        if (self.afterExecute) {
          await self.afterExecute(session);
        }
        cb(err);
      }
      // eslint-disable-next-line no-underscore-dangle
      self._runTaskQueue(session, (err, history) => {
        // eslint-disable-next-line no-underscore-dangle
        summaryMap[session._host] = { error: err, history };
        cb2(err);
      });
    }

    function completedCallback() {
      if (callback) callback(summaryMap);
    }

    if (options.series) {
      asyncFunc.mapSeries(sessions, iterator, completedCallback);
    }
    else {
      asyncFunc.map(sessions, iterator, completedCallback);
    }
  };
}
