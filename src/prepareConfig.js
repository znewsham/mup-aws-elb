export default function prepareConfig(config) {
  if (!config.app.deployment) {
    config.app.deployment = {};
  }
  if (!config.app.deployment.balancer) {
    config.app.deployment.balancer = {};
  }
  if (!config.app.deployment.balancer.name) {
    config.app.deployment.balancer.name = config.app.name;
  }
  if (config.app.deployment.balancer.http === undefined) {
    config.app.deployment.balancer.http = true;
  }
  if (!config.app.deployment.balancer.availabilityZones) {
    config.app.deployment.balancer.availabilityZones = ['a', 'b', 'c'];
  }
  if (!config.app.deployment.balancer.tags) {
    config.app.deployment.balancer.tags = [];
  }
  if (!config.app.deployment.balancer.securityGroups) {
    config.app.deployment.balancer.securityGroups = [];
  }
  if (!config.app.deployment.balancer.targetGroups) {
    config.app.deployment.balancer.targetGroups = [{}];
  }
  if (!config.app.deployment.balancer.waitOnCreateTimeoutSeconds) {
    config.app.deployment.balancer.waitOnCreateTimeoutSeconds = 120;
  }
  if (!config.app.deployment.balancer.rules) {
    config.app.deployment.balancer.rules = [];
  }
  if (!config.app.deployment.balancer.listeners) {
    config.app.deployment.balancer.listeners = [];
    if (config.app.deployment.balancer.http && !config.app.deployment.balancer.listeners.find(l => l.port === 80)) {
      config.app.deployment.balancer.listeners.push({
        port: 80,
        protocol: 'HTTP',
        rules: config.app.deployment.balancer.rules
      });
    }
    if (config.app.deployment.balancer.ssl && !config.app.deployment.balancer.listeners.find(l => l.port === 443)) {
      config.app.deployment.balancer.listeners.push({
        port: 443,
        protocol: 'HTTPS',
        certificates: [{
          certificateArn: config.app.deployment.balancer.ssl.certificateArn
        }],
        rules: config.app.deployment.balancer.rules
      });
    }
  }
  config.servers = {};
  config.app.servers = {};


  config.app.deployment.balancer.targetGroups.forEach((tg) => {
    if (!tg.attributes) {
      tg.attributes = [];
    }
    if (!tg.attributes.find(a => a.key === 'stickiness.enabled')) {
      tg.attributes.push({
        key: 'stickiness.enabled',
        value: 'true'
      });
    }
    if (!tg.attributes.find(a => a.key === 'deregistration_delay.timeout_seconds')) {
      tg.attributes.push({
        key: 'deregistration_delay.timeout_seconds',
        value: '5'
      });
    }
    if (!tg.name) {
      // NOTE: the app-name should be unique, the balancer name will probably be shared
      tg.name = config.app.name;
    }
    if (!tg.port) {
      tg.port = config.app.env.PORT ? parseInt(config.app.env.PORT, 10) : 80;
    }
    if (!tg.healthCheckPath && config.app.env.ROOT_URL.replace(/https?:\/\//, '').indexOf('/') !== -1) {
      tg.healthCheckPath = `/${config.app.env.ROOT_URL.replace(/https?:\/\//, '').split('/').slice(1).join('/')}`;
    }
    if (!tg.protocol) {
      tg.protocol = 'HTTP';
    }
    tg.targetType = 'ip';
  });

  config.app.deployment.balancer.listeners.forEach((l) => {
    if (!l.targetGroupName && !l.targetGroupArn) {
      l.targetGroupName = config.app.deployment.balancer.name;
    }
  });
  config.app.deployment.balancer.securityGroups = config.app.deployment.balancer.securityGroups.map((g) => {
    if ((typeof g) === 'string') {
      return (g.indexOf('sg-') === 0 ? { groupId: g } : { groupName: g });
    }
    return g;
  });
  config.app.deployment.balancer.availabilityZones = config.app.deployment.balancer.availabilityZones.map(z => z.substr(-1, 1));


  if (!config.app.deployment.launchTemplate) {
    config.app.deployment.launchTemplate = {};
  }
  if (!config.app.deployment.launchTemplate.availabilityZones) {
    config.app.deployment.launchTemplate.availabilityZones = config.app.deployment.balancer.availabilityZones;
  }
  if (!config.app.deployment.launchTemplate.instanceName) {
    config.app.deployment.launchTemplate.instanceName = config.app.name;
  }
  if (!config.app.deployment.launchTemplate.tags) {
    config.app.deployment.launchTemplate.tags = [];
  }
  if (!config.app.deployment.launchTemplate.securityGroups) {
    config.app.deployment.launchTemplate.securityGroups = [];
  }
  config.app.deployment.launchTemplate.securityGroups = config.app.deployment.launchTemplate.securityGroups.map((g) => {
    if ((typeof g) === 'string') {
      return (g.indexOf('sg-') === 0 ? { groupId: g } : { groupName: g });
    }
    return g;
  });
  return config;
}
