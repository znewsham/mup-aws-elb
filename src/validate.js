import joi from 'joi';

const schema = joi.object().keys({
  name: joi.string().min(1).required(),
  path: joi.string().min(1).required(),
  enableUploadProgressBar: joi.boolean(),
  type: joi.string().required(),
  buildOptions: joi.object().keys({
    serverOnly: joi.bool(),
    debug: joi.bool(),
    buildLocation: joi.string(),
    mobileSettings: joi.object(),
    server: joi.string().uri(),
    allowIncompatibleUpdates: joi.boolean(),
    executable: joi.string()
  }),
  // The meteor plugin adds the docker object, which is a bug in mup
  docker: joi.object(),
  env: joi.object(),
  region: joi.string(),
  servers: joi.object(),
  auth: joi.object().keys({
    id: joi.string().required(),
    secret: joi.string().required(),
    instanceUsername: joi.string().required(),
    instancePem: joi.string(),
    instancePassword: joi.string()
  }).required(),
  deployment: joi.object().keys({
    balancer: joi.object().keys({
      ensure: joi.boolean(),
      name: joi.string(),
      waitOnCreateTimeoutSeconds: joi.number(),
      ssl: joi.object().keys({
        certificateArn: joi.string()
      }),
      http: joi.boolean(),
      availabilityZones: joi.array().items(joi.string()),
      rules: joi.array().items(joi.object().keys({
        conditions: joi.array().items(joi.object().keys({
          field: joi.string(),
          value: joi.string()
        })),
        priority: joi.string()
      })),
      listeners: joi.array().items(joi.object().keys({
        targetGroupName: joi.string(),
        targetGroupArn: joi.string(),
        port: joi.number(),
        protocol: joi.string().allow(['HTTP', 'HTTPS']),
        certificates: joi.array().items(joi.object().keys({
          certificateArn: joi.string()
        })),
        rules: joi.array().items(joi.object().keys({
          conditions: joi.array().items(joi.object().keys({
            field: joi.string(),
            value: joi.string()
          })),
          priority: joi.string()
        }))
      }).without('targetGroupArn', 'targetGroupName')),
      targetGroups: joi.array().items(joi.object().keys({
        port: joi.number(),
        name: joi.string(),
        healthCheckPath: joi.string(),
        protocol: joi.string(),
        targetType: 'ip',
        attributes: joi.array().items(joi.object().keys({
          key: joi.string(),
          value: joi.string()
        }))
      })),
      tags: joi.array().items(joi.object().keys({
        key: joi.string(),
        value: joi.string()
      })),
      securityGroups: joi.array().items([joi.string(), joi.object().keys({
        groupName: joi.string(),
        inboundGroupNames: joi.array().items(joi.string()),
        groupId: joi.string()
      }).without('groupName', 'groupId')])
    }).required(),
    baseInstances: joi.number().required(),
    maintainBaseOnDeploy: joi.boolean(),
    launchTemplate: joi.object().keys({
      blockDeviceMappings: joi.array().items(joi.object()),
      availabilityZones: joi.array().items(joi.string()),
      imageId: joi.string(),
      instanceType: joi.string(),
      keyName: joi.string(),
      instanceName: joi.string(),
      securityGroups: joi.array().items([joi.string(), joi.object().keys({
        groupName: joi.string(),
        inboundGroupNames: joi.array().items(joi.string()),
        groupId: joi.string()
      }).without('groupName', 'groupId')]),
      launchTemplateId: joi.string(),
      tags: joi.array().items(joi.object().keys({
        key: joi.string(),
        value: joi.string()
      }))
    })
  }).required()
});

export default function (config, utils) {
  let details = [];
  details = utils.combineErrorDetails(
    details,
    joi.validate(config.app, schema, utils.VALIDATE_OPTIONS)
  );
  if (config.app && config.app.name && config.app.name.length < 4) {
    details.push({
      message: 'must have at least 4 characters',
      path: 'name'
    });
  }

  return utils.addLocation(details, 'app');
}
