import debug from 'debug';
import nodemiral from 'nodemiral';
import {
  prepareBundleSupported, getBuildOptions, setupList, getSessions, deepUcFirst
} from './utils';
import { elbv2, ec2 } from './aws';
import { ensureTargetGroups, getTargetGroups } from './elb/target-groups';
import { getBalancer, waitForBalancer, ensureBalancer } from './elb/balancer';
import { ensureSecurityGroups, getSecurityGroups } from './elb/security-groups';
import {
  getListeners, getRules, ensureListeners, ensureRules
} from './elb/listeners';
//import { logs, restart } from './elb/docker';

const log = debug('mup:module:meteor');

export async function logs(api) {
  const config = api.getConfig().app;
  if (!config) {
    console.error('error: no configs found for meteor');
    process.exit(1);
  }

  const args = api.getArgs();
  console.log(args);
  args.shift();

  const serversToUse = await getServers(api, await getTargetGroupArn(api));
  const newServerSessions = getSessions(api, serversToUse);

  return api.getDockerLogs(config.name, newServerSessions, args);
}


export const name = '';
export async function setup(api) {
  api.oldGetSessions = api.getSessions;
  const serversToUse = await getServers(api, await getTargetGroupArn(api));
  const newServerSessions = getSessions(api, serversToUse);
  api.getSessions = function getSessions1() {
    return newServerSessions;
  };
  return api.runCommand('docker.setup')
  .then(() => api.runCommand('meteor.setup'))
  .then(() => {
    api.getSessions = api.oldGetSessions;
  });
}

export function deploy(api) {
  log('exec => mup meteor deploy');

  // validate settings and config before starting
  api.getSettings();
  const config = api.getConfig().app;
  if (!config) {
    console.error('error: no configs found for meteor');
    process.exit(1);
  }

  return api
  .runCommand('elb.push');
}

export async function getServers(api, targetGroupArn, getInstanceId = false) {
  return elbv2.describeTargetHealth({
    TargetGroupArn: targetGroupArn
  })
  .promise()
  .then((data) => {
    if (api.getOptions()['public-addresses'] || getInstanceId) {
      return ec2.describeInstances({})
      .promise()
      .then((res) => {
        const instances = [];
        res.Reservations.forEach((reservation) => {
          reservation.Instances.forEach((instance) => {
            instances.push(instance);
          });
        });
        return data.TargetHealthDescriptions.map((t) => {
          const instance = instances.find(i => i.PrivateIpAddress === t.Target.Id || i.InstanceId === t.Target.Id);
          return {
            InstanceId: instance && instance.InstanceId,
            State: instance && instance.State.Code,
            SessionIp: instance && instance.PublicIpAddress,
            Id: t.Target.Id,
            Port: t.Target.Port,
            HealthState: t.TargetHealth.State
          };
        });
      });
    }
    return data.TargetHealthDescriptions.map(t => ({
      SessionIp: t.Target.Id,
      Id: t.Target.Id,
      Port: t.Target.Port,
      HealthState: t.TargetHealth.State
    }));
  });
}

export async function getTargetGroupArn(api) {
  const config = api.getConfig().app.deployment.balancer;
  const targetGroupName = api.getOptions()['target-group-name'] || (config.targetGroups.length === 1 ? config.targetGroups[0].name : null);
  let targetGroupArn = api.getOptions()['target-group-arn'] || config.targetGroupArn;
  if (!targetGroupName && !targetGroupArn) {
    throw new Error('When you have configured multiple target groups you must specify which one you are deploying to');
  }
  if (!targetGroupArn) {
    // NOTE: the target groups may not be associated with the load balancer
    targetGroupArn = await elbv2.describeTargetGroups(/* { LoadBalancerArn: balancer.LoadBalancerArn } */)
    .promise()
    .then(({ TargetGroups }) => TargetGroups.find(tg => tg.TargetGroupName === targetGroupName).TargetGroupArn);
  }
  return targetGroupArn;
}

export async function servers(api) {
  const registeredServers = await getServers(api, await getTargetGroupArn(api));
  console.log(JSON.stringify(registeredServers, null, 2));
}

export async function registerTargets(api, newTargetIds) {
  const port = api.getConfig().app.env.PORT || 80;
  elbv2.deregisterTargets({
    TargetGroupArn: await getTargetGroupArn(api),
    Targets: newTargetIds.map(id => ({ Id: id, Port: port }))
  })
  .promise();
}

export function addStartAppTask(list, api) {
  const appConfig = api.getConfig().app;
  const isDeploy = api.commandHistory.find(({ name: commandName }) => commandName === 'elb.deploy');

  if (isDeploy) {
    list.executeScript('Start Meteor', {
      script: api.resolvePath(__dirname, 'assets/meteor-start.sh'),
      vars: {
        appName: appConfig.name,
        removeImage: isDeploy && !prepareBundleSupported(appConfig.docker)
      }
    });
  }

  return list;
}

async function pushToServers({
  api, sessions, series = true, beforeExecute = null, afterExecute = null, meteorStart = false
}) {
  const config = api.getConfig().app;
  const list = nodemiral.taskList('Pushing Meteor App');
  const buildOptions = getBuildOptions(api);
  const bundlePath = api.resolvePath(buildOptions.buildLocation, 'bundle.tar.gz');
  await setupList(list);
  list.beforeExecute = beforeExecute;
  list.afterExecute = afterExecute;
  list.copy('Pushing Meteor App Bundle to the Server', {
    src: bundlePath,
    dest: `/opt/${config.name}/tmp/bundle.tar.gz`,
    progressBar: config.enableUploadProgressBar
  });
  if (prepareBundleSupported(config.docker)) {
    list.executeScript('Prepare Bundle', {
      script: api.resolvePath(
        __dirname,
        'assets/prepare-bundle.sh'
      ),
      vars: {
        appName: config.name,
        dockerImage: config.docker.image,
        env: config.env,
        buildInstructions: config.docker.buildInstructions || []
      }
    });
  }

  config.log = config.log || {
    opts: {
      'max-size': '100m',
      'max-file': 10
    }
  };

  config.nginx = config.nginx || {};

  if (config.dockerImageFrontendServer) {
    config.docker.imageFrontendServer = config.dockerImageFrontendServer;
  }
  if (!config.docker.imageFrontendServer) {
    config.docker.imageFrontendServer = 'meteorhacks/mup-frontend-server';
  }

  // If imagePort is not set, go with port 80 which was the traditional
  // port used by kadirahq/meteord and meteorhacks/meteord
  config.docker.imagePort = config.docker.imagePort || 80;

  if (config.ssl) {
    config.ssl.port = config.ssl.port || 443;
  }
  return api.runTaskList(list, sessions, {
    series,
    verbose: api.verbose
  });
}

export async function cleanup(api) {
  const targetGroupArn = await getTargetGroupArn(api);
  const registeredServers = await getServers(api, targetGroupArn, true);
  const targetHealths = await elbv2.describeTargetHealth({ TargetGroupArn: targetGroupArn })
  .promise().then(({ TargetHealthDescriptions }) => TargetHealthDescriptions);
  return Promise.all(targetHealths.map((target) => {
    const server = registeredServers.find(aServer => aServer.Id === target.Target.Id);
    if (target.TargetHealth.Code !== 'healthy' && (!server || !server.InstanceId || server.State === 'terminated' || api.getOptions().unterminated)) {
      console.log(`deregistering ${server.Id}:${server.Port}`);
      const promise = elbv2.deregisterTargets({
        TargetGroupArn: targetGroupArn,
        Targets: [{
          Id: server.Id,
          Port: server.Port
        }]
      })
      .promise();
      if (server && server.InstanceId && server.State !== 'terminated') {
        console.log(`terminating ${server.Id}:${server.Port} (${server.InstanceId})`);
        return promise.then(() => ec2.terminateInstances({ InstanceIds: [server.InstanceId] }).promise());
      }
      return promise;
    }
    return Promise.resolve();
  }));
}

export async function getDefaultVpcId() {
  return ec2.describeVpcs({}).promise().then(({ Vpcs }) => Vpcs.filter(vpc => vpc.IsDefault === true)[0].VpcId);
}

export async function ensureBaseInstances(api) {
  const config = api.getConfig().app;
  if (config.deployment.baseInstances) {
    const targetGroupArn = await getTargetGroupArn(api);
    const instances = await getServers(api, targetGroupArn);
    api.getOptions().instances = config.deployment.baseInstances - instances.length;
    if (api.getOptions().instances > 0) {
      return api.runCommand('elb.spinupInstances');
    }
  }
}

export async function spinupInstances(api) {
  const config = api.getConfig();
  const instancesToSpinup = parseInt(api.getOptions().instances, 10);
  if (Number.isNaN(instancesToSpinup) || instancesToSpinup <= 0) {
    return;
  }
  console.log(`Spinning up ${instancesToSpinup} instances`);
  const defaultVpcId = await getDefaultVpcId();
  const securityGroups = await ensureSecurityGroups(defaultVpcId, config.app.deployment.launchTemplate.securityGroups, config.app.env.PORT || 80);
  const zones = await ec2.describeSubnets({
    Filters: [
      {
        Name: 'vpc-id',
        Values: [
          defaultVpcId
        ]
      }
    ]
  }).promise().then(({ Subnets }) => Subnets);
  const ourZones = zones.filter(zone => config.app.deployment.launchTemplate.availabilityZones.indexOf(zone.AvailabilityZone.substr(-1, 1)) !== -1);
  const promises = [];
  for (let i = 0; i < instancesToSpinup; i++) {
    promises.push(ec2.runInstances({
      MinCount: 1,
      MaxCount: 1,
      BlockDeviceMappings: (config.app.deployment.launchTemplate.blockDeviceMappings || []).map(bs => deepUcFirst(bs)),
      KeyName: config.app.deployment.launchTemplate.keyName,
      ImageId: config.app.deployment.launchTemplate.imageId,
      InstanceType: config.app.deployment.launchTemplate.instanceType,
      SecurityGroupIds: securityGroups,
      SubnetId: ourZones[Math.floor(Math.random() * ourZones.length)].SubnetId,
      TagSpecifications: [
        {
          ResourceType: 'instance',
          Tags: (config.app.deployment.launchTemplate.tags || [])
          .concat([{ key: 'Name', value: config.app.deployment.launchTemplate.instanceName }])
          .map(t => ({ Key: t.key, Value: t.value }))
        },
        {
          ResourceType: 'volume',
          Tags: (config.app.deployment.launchTemplate.tags || [])
          .concat([{ key: 'Name', value: `${config.app.deployment.launchTemplate.instanceName}-root` }])
          .map(t => ({ Key: t.key, Value: t.value }))
        }
      ]
    })
    .promise());
  }
  return Promise.all(promises)
  .then((allResponses) => {
    let createdInstances = [];
    allResponses.forEach((response) => {
      createdInstances = createdInstances.concat(response.Instances);
    });
    console.log('waiting for new instances to be ready');
    return new Promise((resolve) => {
      const interval = setInterval(() => {
        ec2.describeInstances({
          InstanceIds: createdInstances.map(i => i.InstanceId)
        })
        .promise()
        .then((res) => {
          let Instances = [];
          res.Reservations.forEach((res1) => {
            Instances = Instances.concat(res1.Instances);
          });
          // NOTE: wait for the instances to have an IP address
          //       (either public or private, depending on where we are deploying from)
          if (createdInstances.length === Instances.length && !Instances.some(i => !(api.getOptions()['public-addresses'] ? i.PublicIpAddress : i.PrivateIpAddress))) {
            clearInterval(interval);
            console.log('Instances have IP address available - waiting 60 seconds so we hopefully have a port 22 socket');
            setTimeout(() => {
              console.log('Instances ready - we hope');
              api.newInstances = Instances;
              resolve(Instances);
            }, 60000);
          }
        });
      }, 5000);
    });
  });
}

export async function pushToNewServers(api) {
  const { newInstances } = api;
  await pushToServers({
    api,
    sessions: api.getSessions(),
    series: true,
    async afterExecute(session) {
      const oldSessions = api.getSessions;
      api.getSessions = function _getSessions() {
        return [session];
      };
      api.getSessions.skip = true;
      await api.runCommand('meteor.envconfig')
      .then(() => api.runCommand('meteor.start'))
      .then(() => {
        api.getSessions = oldSessions;
      });
      const targetGroupArn = await getTargetGroupArn(api);
      // NOTE: find the server instance for this session, matching
      //       by either public or private IP depending on where we're deploying from
      // eslint-disable-next-line no-underscore-dangle
      const server = newInstances.find(i => (api.getOptions()['public-addresses'] ? i.PublicIpAddress : i.PrivateIpAddress) === session._host);

      // NOTE: register this target
      return elbv2.registerTargets({
        TargetGroupArn: targetGroupArn,
        Targets: [{
          Id: server.PrivateIpAddress,
          Port: api.getConfig().app.env.PORT || 80
        }]
      })
      .promise()
      .then(() => new Promise((resolve) => {
        // NOTE: wait for the new target to be healthy
        const interval = setInterval(() => {
          elbv2.describeTargetHealth({
            TargetGroupArn: targetGroupArn
          })
          .promise()
          .then((data) => {
            console.log(`waiting for target ${server.PrivateIpAddress}:${server.Port} to be healthy`);
            const serverHealth = data.TargetHealthDescriptions.find(t => t.Target.Id === server.PrivateIpAddress);
            if (serverHealth && (serverHealth.TargetHealth.State === 'healthy' || serverHealth.TargetHealth.State === 'unused')) {
              console.log(`target ${server.PrivateIpAddress}:${server.Port} is ${serverHealth.TargetHealth.State}`);
              clearInterval(interval);
              resolve();
            }
          });
        }, 5000);
      }));
    }
  });
}

export async function beforeEachServerDeploy(api, targetGroupArn, session, serversAvailable, healthStatuses) {
  // eslint-disable-next-line no-underscore-dangle
  const server = serversAvailable.find(aServer => aServer.SessionIp === session._host);
  healthStatuses = healthStatuses.filter(t => t.Target.Id !== server.Id && (t.TargetHealth.State === 'healthy' || t.TargetHealth.State === 'unused'));
  if (healthStatuses.length === 0 && !api.getOptions()['downtime-acceptable']) {
    throw new Error('Deploying to this server would result in down time - use --downtime-acceptable if that is acceptable');
  }
  return elbv2.deregisterTargets({
    TargetGroupArn: targetGroupArn,
    Targets: [{
      Id: server.Id,
      Port: server.Port
    }]
  })
  .promise();
}

export async function push(api) {
  log('exec => mup meteor push');
  const targetGroupArn = await getTargetGroupArn(api);
  let healthStatuses = await elbv2.describeTargetHealth({
    TargetGroupArn: targetGroupArn
  })
  .promise()
  .then(data => data.TargetHealthDescriptions);
  const serversAvailable = await getServers(api, targetGroupArn);
  const currentServerSessions = getSessions(api, serversAvailable);
  return pushToServers({
    meteorStart: true,
    api,
    sessions: currentServerSessions,
    series: true,
    beforeExecute(session) {
      return beforeEachServerDeploy(api, targetGroupArn, session, serversAvailable, healthStatuses);
    },
    async afterExecute(session) {
      const oldSessions = api.getSessions;
      api.getSessions = function _getSessions() {
        return [session];
      };
      api.getSessions.skip = true;
      await api.runCommand('meteor.envconfig')
      .then(() => api.runCommand('meteor.start'))
      .catch(err => console.log(err))// NOTE: we redeploy the old version on failure, so we want to catch the error.
      .then(() => {
        api.getSessions = oldSessions;
      });
      // eslint-disable-next-line no-underscore-dangle
      const server = serversAvailable.find(aServer => aServer.SessionIp === session._host);
      console.log(`registering target ${server.Id}:${server.Port}`);
      return elbv2.registerTargets({
        TargetGroupArn: targetGroupArn,
        Targets: [{
          Id: server.Id,
          Port: server.Port
        }]
      })
      .promise()
      .then(() => {
        console.log(`waiting for target ${server.Id}:${server.Port} to be healthy`);
        return new Promise((resolve) => {
          const interval = setInterval(() => {
            elbv2.describeTargetHealth({
              TargetGroupArn: targetGroupArn
            })
            .promise()
            .then((data) => {
              const serverHealth = data.TargetHealthDescriptions.find(t => t.Target.Id === server.Id);
              if (serverHealth && (serverHealth.TargetHealth.State === 'healthy' || serverHealth.TargetHealth.State === 'unused')) {
                console.log(`target ${server.Id}:${server.Port} is ${serverHealth.TargetHealth.State}`);
                healthStatuses = healthStatuses.concat(serverHealth);
                clearInterval(interval);
                resolve();
              }
            });
          }, 5000);
        });
      });
    }
  });
}

// NOTE: balancer variable is used a lot so we call the function balancerCommand to avoid no-shadow eslint errors
export async function balancerCommand(api) {
  const balancer = await getBalancer(api);
  if (!balancer) {
    throw new Error('Load Balancer Not Found');
  }

  const securityGroups = await getSecurityGroups({ GroupIds: balancer.SecurityGroups });
  const listeners = await getListeners({ LoadBalancerArn: balancer.LoadBalancerArn });
  return Promise.all(listeners.map(l => new Promise((resolve) => {
    l.DefaultAction = l.DefaultActions[0];
    getTargetGroups({ TargetGroupArns: [l.DefaultAction.TargetGroupArn] })
    .then((groups) => {
      l.DefaultAction.TargetGroup = groups[0];
      return getRules({ ListenerArn: l.ListenerArn });
    })
    .then((rules) => {
      l.Rules = rules;
      return Promise.all(l.Rules.map(r => new Promise((innerResolve) => {
        getTargetGroups({ TargetGroupArns: [r.Actions[0].TargetGroupArn] })
        .then((groups) => {
          r.Actions[0].TargetGroup = groups[0];
          innerResolve();
        });
      })));
    })
    .then(() => {
      resolve();
    });
  })))
  .then(() => {
    console.log(JSON.stringify({
      state: balancer.State.Code,
      arn: balancer.LoadBalancerArn,
      name: balancer.LoadBalancerName,
      dns: balancer.DNSName,
      availabilityZones: balancer.AvailabilityZones.map(z => z.ZoneName),
      securityGroups: securityGroups.map(g => ({ name: g.GroupName, id: g.GroupId })),
      listeners: listeners.map(l => ({
        port: l.Port,
        protocol: l.Protocol,
        defaultTargetGroup: l.DefaultAction.TargetGroup.TargetGroupName,
        rules: l.Rules.map(r => ({
          priority: r.Priority,
          conditions: r.Conditions.map(c => ({ field: c.Field, values: c.Values })),
          targetGroup: r.Actions[0].TargetGroup.TargetGroupName
        })).sort((r1, r2) => r1.priority - r2.priority)
      }))
    }, null, 2));
  });
}

export async function configBalancer(api) {
  const config = api.getConfig().app.deployment.balancer;
  if (config.ensure === false) {
    console.log('Skipping balancer configuration');
    return;
  }
  const defaultVpcId = await getDefaultVpcId();

  const securityGroups = await ensureSecurityGroups(defaultVpcId, config.securityGroups, api.getConfig().app.env.PORT || 80);

  const zones = await ec2.describeSubnets().promise().then(({ Subnets }) => Subnets);
  const ourZones = zones.filter(zone => config.availabilityZones.indexOf(zone.AvailabilityZone.substr(-1, 1)) !== -1);
  const params = {
    Name: config.name,
    Subnets: ourZones.map(z => z.SubnetId),
    SecurityGroups: securityGroups,
    Type: 'application'
  };
  if (config.tags.length) {
    params.Tags = config.tags.map(t => ({ Key: t.key, Value: t.value }));
  }
  const balancer = await ensureBalancer(api, params);
  const newGroupIds = {};
  balancer.SecurityGroups.forEach((g) => {
    newGroupIds[g] = true;
  });
  securityGroups.forEach((g) => {
    newGroupIds[g] = true;
  });
  await elbv2.setSecurityGroups({
    LoadBalancerArn: balancer.LoadBalancerArn,
    SecurityGroups: Object.keys(newGroupIds)
  })
  .promise();
  await ensureTargetGroups(balancer.LoadBalancerArn, config.targetGroups, defaultVpcId);
  await ensureListeners(balancer.LoadBalancerArn, await getTargetGroupArn(api), config.listeners);
  return waitForBalancer(balancer.LoadBalancerArn, config.waitOnCreateTimeoutSeconds);
}
